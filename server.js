<<<<<<< 180450d1b2716eece656a7fd3fd5578e434c3739
=======

//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//
>>>>>>> 3070706c4e70b472fa7c1df537b8ce83a4ab0f81
var http = require('http');
var path = require('path');
var async = require('async');
var socketio = require('socket.io');
var express = require('express');

//
// ## SimpleServer `SimpleServer(obj)`
//
// Creates a new instance of SimpleServer with the following options:
//  * `port` - The HTTP port to listen on. If `process.env.PORT` is set, _it overrides this value_.
//
var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

router.use(express.static(path.resolve(__dirname, 'client')));
var locations = [];
var messages = [];
var sockets = [];

io.sockets.on('connection', function(socket) {
  locations.forEach(function(data) {
  });
  sockets.push(socket);


  socket.on('disconnect', function() {
    sockets.splice(sockets.indexOf(socket), 1);
  });

  socket.on('shuttlePosition', function(crd) {
    console.log("***********RECEIVED COORDINATES FROM SHUTTLE************");
    console.log(crd);
    locations.push(crd);
    broadcast('shuttleMoving', crd);
  });

});


function broadcast(event, data) {
  sockets.forEach(function(socket) {
    socket.emit(event, data);
    console.log("***********BROADCASTING FROM SERVER************");
  });
}

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function() {
  var addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});
